<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\libro;
class LibroController extends Controller
{

    public function __construct()
    {
        //https://gitlab.com/techhome/basiclaraveltest
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $libros = libro::all();
        $array = array (
            'libros'=>$libros
        );
        return view('libro')->with($array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Libro.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     //https://gitlab.com/techhome/basiclaraveltest   
 $this->validate($request,[ 'nombre'=>'required', 'detalle'=>'required',
  'nropaginas'=>'required', 'edicion'=>'required', 'autor'=>'required',
  'price'=>'required']);
    libro::create($request->all());
 return redirect()->route('libro.index')->with('correcto','Registro creado satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $libro = libro::find($id);
        $array=array ('libro'=>$libro);
        return view('Libro.edit')->with($array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[ 'nombre'=>'required', 'detalle'=>'required',
        'nropaginas'=>'required', 'edicion'=>'required', 'autor'=>'required',
        'price'=>'required']);
        /*$libro = libro::find($id);
        $libro->nombre = $request->input("nombre");
        $libro->detalle = $request->input("detalle");
        $libro->nropaginas = $request->input("nropaginas");
        $libro->edicion = $request->input("edicion");
        $libro->autor = $request->input("autor");
        $libro->price = $request->input("price");
        $libro->save();*/

        libro::find($id)->update($request->all());
        return redirect()->route('libro.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        libro::find($id)->delete();
        return redirect()->route('libro.index');
    }
}
