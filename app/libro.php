<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class libro extends Model
{
    protected $table="libro";
    protected $fillable = ['id',
    'nombre','detalle',
    'nropaginas','edicion','autor','price'];
}
