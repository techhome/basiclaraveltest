@extends('layouts.app')
@section('content')
<div class="row">
  <section class="content">
    <div class=" col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="pull-left"><h3>Lista Libros</h3></div>
          <div class="pull-right">
            <div class="btn-group">
              <a href="{{ route('libro.create') }}" class="btn btn-info" >Añadir Libro</a>
            </div>
          </div>
          <div class="table-container">
            <table id="mytable" class="table table-bordred table-striped">
             <thead>
               <th>Nombre</th>
               <th>Resumen</th>
               <th>No. Páginas</th>
               <th>Edicion</th>
               <th>Autor</th>
               <th>Precio</th>
               <th>Editar</th>
               <th>Eliminar</th>
             </thead>
             <tbody>
              @if($libros->count())  
              @foreach($libros as $libro)  
              <tr>
                <td>{{$libro->nombre}}</td>
                <td>{{$libro->detalle}}</td>
                <td>{{$libro->nropaginas}}</td>
                <td>{{$libro->edicion}}</td>
                <td>{{$libro->autor}}</td>
                <td>{{$libro->price}}</td>
                    <td><a href="{{ route('libro.edit', $libro->id) }}">edit</a></td>
                <td>
                <form method="POST" action="{{ route('libro.destroy',$libro->id)}}">
                    @csrf
                    @method('DELETE')
                        <input type="submit" value="eliminar">
                    </form>
                 </td>
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
            </tbody>

          </table>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
