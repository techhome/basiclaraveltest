@extends('layouts.app')
@section('content')
<form method="POST" action="{{route('libro.update', $libro->id)}}">
        @csrf
        @method("PUT")
<input type="text" name="nombre" placeholder="nombre" value="{{ $libro->nombre }}" />
        <input type="text" name="detalle" placeholder="detalle" value="{{ $libro->detalle }}" />
        <input type="text" name="nropaginas" placeholder="nropaginas" value="{{ $libro->nropaginas }}" />
        <input type="text" name="edicion" placeholder="edition" value="{{ $libro->edicion }}" />
        <input type="text" name="autor" placeholder="autor" value="{{ $libro->autor }}" />
        <input type="text" name="price" placeholder="price" value="{{ $libro->price }}" />
        <input type="submit" value="Guardar"  />
    </form>
@endsection